<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1">
  <context>
    <name />
    <message>
      <location filename="../emid/global_parameters.py" line="155" />
      <location filename="../emid/global_parameters.py" line="152" />
      <location filename="../emid/global_parameters.py" line="84" />
      <source>custom</source>
      <translation type="unfinished" />
    </message>
  </context>
  <context>
    <name>mainWin</name>
    <message>
      <location filename="../emid/__main__.py" line="128" />
      <source>emid</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../emid/__main__.py" line="144" />
      <source>-</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../emid/__main__.py" line="146" />
      <source>Transducers</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../emid/__main__.py" line="150" />
      <source>Preferences</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../emid/__main__.py" line="154" />
      <source>Manual (html)</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../emid/__main__.py" line="158" />
      <source>Manual (pdf)</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../emid/__main__.py" line="427" />
      <location filename="../emid/__main__.py" line="162" />
      <source>About emid</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../emid/__main__.py" line="174" />
      <source>Setup Listener</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../emid/__main__.py" line="247" />
      <location filename="../emid/__main__.py" line="238" />
      <location filename="../emid/__main__.py" line="181" />
      <source>Start</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../emid/__main__.py" line="205" />
      <source>Happy</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../emid/__main__.py" line="205" />
      <source>Sad</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../emid/__main__.py" line="205" />
      <source>Neutral</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../emid/__main__.py" line="205" />
      <source>Angry</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../emid/__main__.py" line="205" />
      <source>Scared</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../emid/__main__.py" line="377" />
      <location filename="../emid/__main__.py" line="290" />
      <location filename="../emid/__main__.py" line="252" />
      <location filename="../emid/__main__.py" line="234" />
      <source>Next</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../emid/__main__.py" line="267" />
      <location filename="../emid/__main__.py" line="236" />
      <source>Finished</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../emid/__main__.py" line="377" />
      <location filename="../emid/__main__.py" line="271" />
      <location filename="../emid/__main__.py" line="260" />
      <location filename="../emid/__main__.py" line="241" />
      <source>Running</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../emid/__main__.py" line="321" />
      <location filename="../emid/__main__.py" line="244" />
      <source>Listener ID: </source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../emid/__main__.py" line="326" />
      <source>Choose trial list file</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../emid/__main__.py" line="347" />
      <location filename="../emid/__main__.py" line="326" />
      <source>CSV (*.csv *CSV *Csv);;All Files (*)</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../emid/__main__.py" line="347" />
      <source>Choose file to write results</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../emid/__main__.py" line="428" />
      <source>&lt;b&gt;emid - Python app for sound localization experiments&lt;/b&gt; &lt;br&gt;
                              - version: {0}; &lt;br&gt;
                              - build date: {1} &lt;br&gt;
                              &lt;p&gt; Copyright &amp;copy; 2022-2023 Samuele Carcagno. &lt;a href="mailto:sam.carcagno@gmail.com"&gt;sam.carcagno@gmail.com&lt;/a&gt; 
                              All rights reserved. &lt;p&gt;
                              This program is free software: you can redistribute it and/or modify
                              it under the terms of the GNU General Public License as published by
                              the Free Software Foundation, either version 3 of the License, or
                              (at your option) any later version.
                              &lt;p&gt;
                              This program is distributed in the hope that it will be useful,
                              but WITHOUT ANY WARRANTY; without even the implied warranty of
                              MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
                              GNU General Public License for more details.
                              &lt;p&gt;
                              You should have received a copy of the GNU General Public License
                              along with this program.  If not, see &lt;a href="http://www.gnu.org/licenses/"&gt;http://www.gnu.org/licenses/&lt;/a&gt;
                              &lt;p&gt;Python {2} - {3} {4} compiled against Qt {5}, and running with Qt {6} on {7}</source>
      <translation type="unfinished" />
    </message>
  </context>
  <context>
    <name>preferencesDialog</name>
    <message>
      <location filename="../emid/dialog_edit_preferences.py" line="70" />
      <source>Language (requires restart):</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../emid/dialog_edit_preferences.py" line="78" />
      <source>Country (requires restart):</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../emid/dialog_edit_preferences.py" line="85" />
      <source>Start delay (ms):</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../emid/dialog_edit_preferences.py" line="91" />
      <source>CSV separator:</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../emid/dialog_edit_preferences.py" line="117" />
      <source>Transducers:</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../emid/dialog_edit_preferences.py" line="126" />
      <source>Play Command:</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../emid/dialog_edit_preferences.py" line="131" />
      <source>Command:</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../emid/dialog_edit_preferences.py" line="336" />
      <location filename="../emid/dialog_edit_preferences.py" line="242" />
      <location filename="../emid/dialog_edit_preferences.py" line="139" />
      <location filename="../emid/dialog_edit_preferences.py" line="134" />
      <source>custom</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../emid/dialog_edit_preferences.py" line="160" />
      <location filename="../emid/dialog_edit_preferences.py" line="146" />
      <source>Device:</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../emid/dialog_edit_preferences.py" line="177" />
      <source>Buffer Size (samples):</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../emid/dialog_edit_preferences.py" line="194" />
      <source>WAV Manager (requires restart):</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../emid/dialog_edit_preferences.py" line="211" />
      <source>Append silence to each sound (ms):</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../emid/dialog_edit_preferences.py" line="222" />
      <source>Genera&amp;l</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../emid/dialog_edit_preferences.py" line="223" />
      <source>Soun&amp;d</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../emid/dialog_edit_preferences.py" line="355" />
      <source>Warning</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../emid/dialog_edit_preferences.py" line="355" />
      <source>There are unsaved changes. Apply Changes?</source>
      <translation type="unfinished" />
    </message>
  </context>
  <context>
    <name>responseLight</name>
    <message>
      <location filename="../emid/__main__.py" line="559" />
      <location filename="../emid/__main__.py" line="499" />
      <location filename="../emid/__main__.py" line="490" />
      <location filename="../emid/__main__.py" line="476" />
      <location filename="../emid/__main__.py" line="463" />
      <source>Light &amp; Text</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../emid/__main__.py" line="535" />
      <location filename="../emid/__main__.py" line="490" />
      <source>Light</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../emid/__main__.py" line="571" />
      <location filename="../emid/__main__.py" line="524" />
      <location filename="../emid/__main__.py" line="490" />
      <source>Light &amp; Smiley</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../emid/__main__.py" line="593" />
      <location filename="../emid/__main__.py" line="524" />
      <location filename="../emid/__main__.py" line="499" />
      <location filename="../emid/__main__.py" line="490" />
      <source>Light &amp; Text &amp; Smiley</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../emid/__main__.py" line="541" />
      <location filename="../emid/__main__.py" line="499" />
      <source>Text</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../emid/__main__.py" line="578" />
      <location filename="../emid/__main__.py" line="524" />
      <location filename="../emid/__main__.py" line="499" />
      <source>Text &amp; Smiley</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../emid/__main__.py" line="552" />
      <location filename="../emid/__main__.py" line="524" />
      <source>Smiley</source>
      <translation type="unfinished" />
    </message>
  </context>
  <context>
    <name>transducersDialog</name>
    <message>
      <location filename="../emid/dialog_edit_transducers.py" line="62" />
      <source>Transducers</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../emid/dialog_edit_transducers.py" line="62" />
      <source>Max Level</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../emid/dialog_edit_transducers.py" line="67" />
      <source>Rename Transducers</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../emid/dialog_edit_transducers.py" line="70" />
      <source>Change Max Level</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../emid/dialog_edit_transducers.py" line="74" />
      <source>Add Transducers</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../emid/dialog_edit_transducers.py" line="77" />
      <source>Remove Transducers</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../emid/dialog_edit_transducers.py" line="105" />
      <source>Calibration Tone:</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../emid/dialog_edit_transducers.py" line="108" />
      <source>Frequency (Hz)</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../emid/dialog_edit_transducers.py" line="114" />
      <source>Level (dB)</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../emid/dialog_edit_transducers.py" line="120" />
      <source>Duration (ms)</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../emid/dialog_edit_transducers.py" line="126" />
      <source>Ramps (ms)</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../emid/dialog_edit_transducers.py" line="132" />
      <source>Ear:</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../emid/dialog_edit_transducers.py" line="134" />
      <source>Right</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../emid/dialog_edit_transducers.py" line="134" />
      <source>Left</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../emid/dialog_edit_transducers.py" line="134" />
      <source>Both</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../emid/dialog_edit_transducers.py" line="138" />
      <source>Play</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../emid/dialog_edit_transducers.py" line="143" />
      <source>Stop</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../emid/dialog_edit_transducers.py" line="165" />
      <source>Edit Transducers</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../emid/dialog_edit_transducers.py" line="264" />
      <location filename="../emid/dialog_edit_transducers.py" line="261" />
      <location filename="../emid/dialog_edit_transducers.py" line="247" />
      <location filename="../emid/dialog_edit_transducers.py" line="190" />
      <location filename="../emid/dialog_edit_transducers.py" line="177" />
      <source>Warning</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../emid/dialog_edit_transducers.py" line="261" />
      <location filename="../emid/dialog_edit_transducers.py" line="177" />
      <source>Only one label can be renamed at a time</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../emid/dialog_edit_transducers.py" line="182" />
      <source>New name:</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../emid/dialog_edit_transducers.py" line="196" />
      <location filename="../emid/dialog_edit_transducers.py" line="183" />
      <source>Input Dialog</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../emid/dialog_edit_transducers.py" line="190" />
      <source>Only one item can be edited at a time</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../emid/dialog_edit_transducers.py" line="195" />
      <source>Level:</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../emid/dialog_edit_transducers.py" line="248" />
      <source>Only one transducer left. Cannot remove!</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../emid/dialog_edit_transducers.py" line="264" />
      <source>Please, select a transducer in the table</source>
      <translation type="unfinished" />
    </message>
  </context>
</TS>
