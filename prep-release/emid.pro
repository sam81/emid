SOURCES	     += ../emid/__main__.py
SOURCES	     += ../emid/audio_manager.py
SOURCES	     += ../emid/dialog_edit_transducers.py
SOURCES	     += ../emid/dialog_edit_preferences.py
SOURCES	     += ../emid/global_parameters.py
SOURCES	     += ../emid/sndlib.py
TRANSLATIONS += emid_it.ts emid_fr.ts emid_es.ts emid_el.ts emid_en_US.ts emid_en_GB.ts
