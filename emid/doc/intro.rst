****************************
``emid``
****************************

``emid`` is program for voice emotion identification tests. The interface is shown in Figure :ref:`fig-emid_screenshot`.

.. _fig-emid_screenshot:

.. figure:: Figures/emid_screenshot.png
   :scale: 50%
   :alt: Screenshot of the ``emid`` interface.

   Screenshot of the ``em_id`` interface.

The experimenter has to provide a trial list (see Section :ref:`sec-experiment_setup`) indicating the voice recordings to be presented on each trial, as well as the phrase spoken in the recording and its intended emotional prosody (happy, sad, neutral, angry, or scared). The software allows presenting the trials and collecting responses from the participant.
